rootProject.name = "SwimmingFatigue"

pluginManagement {
    repositories {
        gradlePluginPortal()
        jcenter()
        mavenCentral()
        mavenLocal()
    }
}